﻿using CLINICAL2.Application.UseCase.Commons.Bases;

namespace CLINICAL2.Application.UseCase.Commons.Exceptions
{
    public class ValidationException : Exception
    {
        public IEnumerable <BaseError> ? Errors { get; }

        public ValidationException() : base()
        {
            Errors = new List<BaseError>();
        }

        public ValidationException(IEnumerable<BaseError>? errors) : base()
        {
            Errors = errors;
        }
    }
}
