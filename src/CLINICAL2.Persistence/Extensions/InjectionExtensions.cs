﻿using CLINICAL2.Application.Interface;
using CLINICAL2.Persistence.Context;
using CLINICAL2.Persistence.Repositories;
using Microsoft.Extensions.DependencyInjection;

namespace CLINICAL2.Persistence.Extensions
{
    public static class InjectionExtensions
    {
        public static IServiceCollection AddInjectionPersistence(this IServiceCollection services)
        {
            services.AddSingleton<ApplicationDbContext>();
            services.AddScoped<IAnalysisRepository, AnalysisRepository>();
            return services;
        }
    }
}
