﻿using CLINICAL2.Application.Dtos.Analysis.Response;
using CLINICAL2.Application.UseCase.Commons.Bases;
using MediatR;

namespace CLINICAL2.Application.UseCase.UseCases.Analysis.Queries.GetByIdQuery
{
    public class GetAnalysisByIdQuery : IRequest<BaseResponse<GetAnalysisByIdResponseDto>>
    {
        public int AnalysisId { get; set; }
    }
}
