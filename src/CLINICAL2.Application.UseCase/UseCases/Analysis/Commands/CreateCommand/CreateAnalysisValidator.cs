﻿using FluentValidation;

namespace CLINICAL2.Application.UseCase.UseCases.Analysis.Commands.CreateCommand
{
    public class CreateAnalysisValidator : AbstractValidator<CreateAnalysisCommand>
    {
        public CreateAnalysisValidator()
        {
            RuleFor(x => x.Name)
                .NotNull().WithMessage("The name fiel can't be null")
                .NotEmpty().WithMessage("The name fiel can't be empty");
        }
    }
}
