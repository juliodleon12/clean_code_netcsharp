﻿using CLINICAL2.Application.Dtos.Analysis.Response;
using CLINICAL2.Application.UseCase.Commons.Bases;
using MediatR;

namespace CLINICAL2.Application.UseCase.UseCases.Analysis.Queries.GetAllQuery
{
    public class GetAllAnalysisQuery : IRequest<BaseResponse<IEnumerable<GetAllAnalysisResponseDto>>>
    {

    }
}
