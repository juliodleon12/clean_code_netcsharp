﻿using CLINICAL2.Application.UseCase.Commons.Bases;
using MediatR;

namespace CLINICAL2.Application.UseCase.UseCases.Analysis.Commands.DeleteCommand
{
    public class DeleteAnalysisCommand : IRequest<BaseResponse<bool>>
    {
        public int AnalysisId { get; set; }
    }
}
