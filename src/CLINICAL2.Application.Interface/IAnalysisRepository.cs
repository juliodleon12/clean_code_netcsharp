﻿using CLINICAL2.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CLINICAL2.Application.Interface
{
    public interface IAnalysisRepository
    {
        Task<IEnumerable<Analysis>> ListAnalysis();
        Task<Analysis> AnalysisById(int analysisId);

        //add this metodo in our repository
        Task<bool> AnalysisRegister(Analysis analysis);

        Task<bool> AnalysisEdit(Analysis analysis);
        Task<bool> AnalysisRemove(int analysisId);
    }
}
