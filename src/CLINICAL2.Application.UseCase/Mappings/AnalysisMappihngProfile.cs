﻿using AutoMapper;
using CLINICAL2.Application.Dtos.Analysis.Response;
using CLINICAL2.Application.UseCase.UseCases.Analysis.Commands.CreateCommand;
using CLINICAL2.Application.UseCase.UseCases.Analysis.Commands.UpdateCommand;
using CLINICAL2.Domain.Entities;

namespace CLINICAL2.Application.UseCase.Mappings
{
    public class AnalysisMappihngProfile : Profile
    {
        public AnalysisMappihngProfile()
        {
            CreateMap<Analysis, GetAllAnalysisResponseDto>()
                .ForMember(x => x.StateAnalysis, x => x.MapFrom(y => y.State == 1 ? "ACTIVO" : "INACTIVO"))
                .ReverseMap();

            CreateMap<Analysis, GetAnalysisByIdResponseDto>()
                .ReverseMap();

            CreateMap<CreateAnalysisCommand, Analysis>();
            CreateMap<UpdateAnalysisCommand, Analysis>();

        }
    }
}
